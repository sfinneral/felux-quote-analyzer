import './App.css';
import { QuoteAnalyzer } from './components/QuoteAnalyzer/QuoteAnalyzer';

function App() {
  return (
    <div className="App">
      <QuoteAnalyzer />
    </div>
  );
}

export default App;
