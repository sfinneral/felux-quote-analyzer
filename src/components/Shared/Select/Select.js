import './Select.css';

export const Select = ({options,onChange,label,id}) => {
    return (
        <div className="selectWrap">
            <label className="selectLabel" htmlFor={id}>{label}</label>
            <select name={id} className="select" onChange={onChange}>
                {
                    options.map(option => <option key={option.value} value={option.value}>{option.text}</option>)
                }
            </select>
        </div>
        
    )
}