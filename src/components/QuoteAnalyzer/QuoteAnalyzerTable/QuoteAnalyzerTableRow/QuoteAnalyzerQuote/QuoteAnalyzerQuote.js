import clsx from 'clsx';
import './QuoteAnalyzerQuote.css';

export const QuoteAnalyzerQuote = ({quote,priceType,product,isHighest,isLowest,onQuoteSelection,purchases}) => {
        const isPurchased = purchases.findIndex(purchase => purchase.PartNo === product.PartNo && purchase.purchase.Company === quote.Company)
        const selected = isPurchased !== -1 ? '*' : null;
    return (
        <button 
            className={clsx('quoteAnalyzerQuote',isHighest && 'highest',isLowest && 'lowest')}
            onClick={() => onQuoteSelection(quote,product)}
        >{quote[priceType]} {selected}</button>
    )
}