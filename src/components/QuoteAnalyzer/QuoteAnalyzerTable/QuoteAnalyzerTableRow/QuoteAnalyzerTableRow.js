import { QuoteAnalyzerQuote } from "./QuoteAnalyzerQuote/QuoteAnalyzerQuote"

export const QuoteAnalyzerTableRow = ({product,priceType,onQuoteSelection,purchases}) => {
    const lowestPriceCompany = [...product.Quotes].sort((a,b) => a[priceType] - b[priceType])[0].Company;
    const highestPriceCompany = [...product.Quotes].sort((a,b) => b[priceType] - a[priceType])[0].Company;

    return (
        <tr>
            <td>{product.Location}</td>
            <td>{product.PartNo}</td>
            <td>{product.Product}</td>
            <td>{product.Weight}</td>
            {
                product.Quotes.map(quote =>
                    <td key={quote.FinalPrice}>
                        <QuoteAnalyzerQuote
                            isLowest={lowestPriceCompany === quote.Company}
                            isHighest={highestPriceCompany === quote.Company}
                            quote={quote}
                            priceType={priceType}
                            product={product}
                            onQuoteSelection={onQuoteSelection}
                            purchases={purchases} />
                    </td>)
            }
        </tr>
    )
}