import "./QuoteAnalyzerTable.css"
import { QuoteAnalyzerTableHeader } from "./QuoteAnalyzerTableHeader/QuoteAnalyzerTableHeader"
import { QuoteAnalyzerTableRow } from "./QuoteAnalyzerTableRow/QuoteAnalyzerTableRow"

export const QuoteAnalyzerTable = ({steelData,priceType,onQuoteSelection,purchases}) => {
    const columns = ['Location','Part #','Steel Product','Weight'];
    const companies = steelData[0].Quotes.map(quotes => quotes.Company)
    return (
        <table className="quoteAnalyzerTable">
            <QuoteAnalyzerTableHeader columns={[...columns,...companies]} />
            <tbody>
                {steelData.map(product => <QuoteAnalyzerTableRow key={product.PartNo} product={product} priceType={priceType} onQuoteSelection={onQuoteSelection} purchases={purchases} />)}
            </tbody>
        </table>
    )
}