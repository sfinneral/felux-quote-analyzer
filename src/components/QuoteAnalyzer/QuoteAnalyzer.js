import * as React from 'react';
import data from '../../datasample.json';
import { Select } from '../Shared/Select/Select';
import {QuoteAnalyzerKPIs} from './QuoteAnalyzerKPIs/QuoteAnalyzerKPIs'
import { QuoteAnalyzerTable } from './QuoteAnalyzerTable/QuoteAnalyzerTable';
import './QuoteAnalyzer.css';

export const QuoteAnalyzer = () => {
    const [steelData,setSteelData] = React.useState(data);
    const [priceType,setPriceType] = React.useState('FinalPrice');
    const [purchases,setPurchases] = React.useState([]);
    // React.useEffect(() => {
    //     if(data){
    //         setSteelData(data);
    //     }
    // },[])
    const options = [
        {
            'text': 'Final Price',
            'value': 'FinalPrice'
        },
        {
            'text': 'Packaging Fee',
            'value': 'PackagingFee'
        },
        {
            'text': 'Freight Fee',
            'value': 'FreightFee'
        }
    ]
    const onQuoteSelection = (quote,product) => {
        const currentPurchases = [...purchases];
        const newPurchase = {...product,'purchase':quote}
        const existingPartIndex = purchases.findIndex(purchase => purchase.PartNo === newPurchase.PartNo)
        const doesPartExist = existingPartIndex !== -1;
        const doesProductExist = doesPartExist && currentPurchases[existingPartIndex].purchase.Company === newPurchase.purchase.Company;
        // if exact product and quote exists
        if(doesProductExist){
            currentPurchases.splice(existingPartIndex,1)
        // if product
        } else if (doesPartExist) {
            currentPurchases.splice(existingPartIndex,1)
            currentPurchases.push(newPurchase)
        } else {
            currentPurchases.push(newPurchase)
        }
        setPurchases(currentPurchases)
    }
    const onPriceTypeChange = (evt) => {
        setPriceType(evt.target.value);
    }
    if(steelData){
        return (
            <div className="quoteAnalyzer">
                <QuoteAnalyzerKPIs purchases={purchases}/>
                <div className="quoteAnalyzerControls">
                    <Select options={options} onChange={onPriceTypeChange} label="Price Type" id="priceType"/>
                </div>
                <QuoteAnalyzerTable steelData={steelData} priceType={priceType} onQuoteSelection={onQuoteSelection} purchases={purchases}/>
            </div>
        )
    } else {
        return <h3>loading...</h3>
    }
}