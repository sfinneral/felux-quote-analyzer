import * as React from 'react';
import {QuoteAnalyzerKPI} from './QuoteAnalyzerKPI/QuoteAnalyzerKPI';
import './QuoteAnalyzerKPIs.css';

export const QuoteAnalyzerKPIs = ({purchases}) => {
    
    const dollarFormat = (number) => {
        return new Intl.NumberFormat('en-US',{style: 'currency', currency: 'USD'}).format(number);
    }

    const getTotal = () => {
        const total = purchases.reduce((accum, current) => {
            return accum + current.Weight
        }, 0)
        return `${total / 1000}k`;
    }
    // Invoice
    // sum of all purchases (weight / 100 * finalPrice)
    const getInvoice = () => {
        const invoice = purchases.reduce((accum, current) => {
            return accum + ((current.Weight / 100) * current.purchase.FinalPrice)
        }, 0)
        return dollarFormat(invoice);
    }

    // Avg $/CWT
    // average of the finalPrice
    const getAveragePrice = () => {
        const priceSum = purchases.reduce((accum, current) => {
            return accum + current.purchase.FinalPrice
        }, 0)
        return dollarFormat(purchases.length ? (priceSum / purchases.length) : 0)
    }
    return (
        <section className="quoteAnalyzerKPIs">
            <QuoteAnalyzerKPI label="Total Pounds" value={getTotal()}/>
            <QuoteAnalyzerKPI label="Invoice" value={getInvoice()}/>
            <QuoteAnalyzerKPI label="Avg $/CWT" value={getAveragePrice()}/>
        </section>
    )
}