import './QuoteAnalyzerKPI.css'

export const QuoteAnalyzerKPI = ({label,value}) => {
    return (
        <figure className="quoteAnalyzerKPI">
            <h3>{label}</h3>
            <p>{value}</p>
        </figure>
    )
}